# Build apps
cd /var/www/ibiza-master;
chown -R www-data:www-data storage;
chown -R www-data:www-data bootstrap/cache;
composer update;
php artisan key:generate;
php artisan migrate;
php artisan passport:install;
php artisan config:clear;
php artisan route:clear;

cd /var/www/metroon_new_be;
chown -R www-data:www-data storage;
chown -R www-data:www-data bootstrap/cache;
composer update;
php artisan key:generate;
php artisan migrate;
php artisan passport:install;
php artisan config:clear;
php artisan route:clear;