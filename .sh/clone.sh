git clone git@bitbucket.org:tech_eppf/ibiza-master.git;
cd ibiza-master;
git pull;
git checkout -B release origin/release;
chmod -R 775 storage;
chmod -R 775 bootstrap/cache;
cd ../;

git clone git@bitbucket.org:tech_eppf/metroon_new_be.git;
cd metroon_new_be;
git pull;
git checkout -B release origin/release;
chmod -R 775 storage;
chmod -R 775 bootstrap/cache;
cd ../;

git clone git@bitbucket.org:tech_eppf/metroon_new_fe.git;
cd metroon_new_fe;
git pull;
git checkout -B release origin/release;
cd ../


git clone git@bitbucket.org:tech_eppf/copacabana_be.git;
cd copacabana_be;
git pull;
git checkout -B production origin/production;
chmod -R 775 storage;
chmod -R 775 bootstrap/cache;
cd ../;

cd docker;
cp .env.example .env;
cp nginx/sites/eppf.local.conf.example nginx/sites/eppf.local.conf;
cd ../;

cd ibiza-master;
cp .env.localdev .env;
cd ../;

cd metroon_new_be;
cp .env.localdev .env;
cd ../;

cd copacabana_be;
cp .env.localdev .env;